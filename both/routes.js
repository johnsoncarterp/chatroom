Router.configure({
    layoutTemplate: "ApplicationLayout"
});

Router.route('/', function () {
    this.render('home');
}, {
    name: "home"
});

Router.route('/post/:_id', function () {
    this.render('post', {
        data: function () {
            return Posts.findOne({
                _id: this.params._id
            });
        }
    });
});